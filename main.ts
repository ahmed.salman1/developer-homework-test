import {
    GetProductsForIngredient,
    GetRecipes
} from "./supporting-files/data-access";
import {NutrientFact, Product, RecipeLineItem, SupplierProduct} from "./supporting-files/models";
import {
    GetCostPerBaseUnit, SumUnitsOfMeasure,
    // GetNutrientFactInBaseUnits
} from "./supporting-files/helpers";
import {
    // RunTest,
    ExpectedRecipeSummary, RunTest
} from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */


function sortObjectAttribute(obj: any) {
    const sortedObj = {};
    const sortedKeys = Object.keys(obj).sort();
    for (const key of sortedKeys) {
      sortedObj[key] = obj[key];
    }
    return sortedObj;
}

function roundOf(nmb: number){
    if (nmb < 1){
        return Number(nmb.toPrecision(1))
    }else{
        return Number(nmb.toFixed(1))
    }
}

let productNutrients : NutrientFact[] = [];
let totalPrice = 0;
// loop on reciepes line items
recipeData.forEach((recipe) => {
    recipe.lineItems.forEach(  ( item : RecipeLineItem) =>{
        //cheapest product we can find here
        const products : Product[] = GetProductsForIngredient(item.ingredient);
        let cheapestPrice: number = 0;
        let cheapProductNutrients: any;
        // all supplier of the product
        products.forEach(( p : Product) =>{
            // get supplier cheapest product for lineitem
            const supplierLowest : SupplierProduct = p.supplierProducts.sort(
                (a : SupplierProduct,b : SupplierProduct) =>  GetCostPerBaseUnit(a)  - GetCostPerBaseUnit(b))[0];
            const supplier_account  =  GetCostPerBaseUnit(supplierLowest);
            if( (supplier_account * item.unitOfMeasure.uomAmount) < cheapestPrice ||  cheapestPrice === 0){
                // if get more cheap price minus previous price added
                totalPrice = totalPrice - cheapestPrice;
                cheapestPrice  =  supplier_account * item.unitOfMeasure.uomAmount;
                cheapProductNutrients = p.nutrientFacts;
                // add checp price tot total
                totalPrice = totalPrice + cheapestPrice;
            }
        });

        //nutrientFacts for the product
        cheapProductNutrients.forEach(( cheap:any ) =>{
            const nutrient = productNutrients.filter((p) => p.nutrientName.includes(cheap.nutrientName));
            // if nutrient already added in a list then update the value else aadd it as it is
            if (nutrient.length > 0){
                try{
                    nutrient[0].quantityAmount.uomAmount += SumUnitsOfMeasure(cheap.quantityPer, cheap.quantityAmount)['uomAmount']-cheap.quantityPer['uomAmount'];
                } catch{
                    nutrient[0].quantityAmount.uomAmount += SumUnitsOfMeasure(cheap.quantityAmount, cheap.quantityPer)['uomAmount']-cheap.quantityPer['uomAmount'];
                }
                nutrient[0].quantityAmount.uomAmount = roundOf(nutrient[0].quantityAmount.uomAmount)
            }
            else {
                // cheap.quantityAmount.uomAmount = SumUnitsOfMeasure(cheap.quantityPer, cheap.quantityAmount)['uomAmount']-cheap.quantityPer['uomAmount'];
                try{
                    cheap.quantityAmount = SumUnitsOfMeasure(cheap.quantityPer, cheap.quantityAmount);
                } catch{
                    cheap.quantityAmount = SumUnitsOfMeasure(cheap.quantityAmount, cheap.quantityPer);
                }
                cheap.quantityAmount.uomAmount -= cheap.quantityPer['uomAmount'];
                productNutrients = productNutrients.concat(cheap);
                cheap.quantityAmount.uomAmount = roundOf(cheap.quantityAmount.uomAmount)
            }
        });    
    });
});
let prodNutrients = {};
productNutrients.forEach(( p ) =>{
    prodNutrients[p.nutrientName] = p
});

/*
 * Reading index 0, as we are running test case for single recepie and checking just one recipe in our test case
 * and we are not allowed to modify anything below line 111
 * */
const recipeName = recipeData[0].recipeName;
recipeSummary[`${recipeName}`] = {'cheapestCost': totalPrice, 'nutrientsAtCheapestCost': prodNutrients}
recipeSummary[`${recipeName}`]['nutrientsAtCheapestCost'] = sortObjectAttribute(recipeSummary[`${recipeName}`]['nutrientsAtCheapestCost']);

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);